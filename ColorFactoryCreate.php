<?php
namespace Colors;

/**
 * Class ColorFactoryCreate
 * @package Colors
 */
class ColorFactoryCreate extends BaseView
{
    /**
     * Overrides parent method to add a create-centric sentence
     */
    public function greeting(){
        parent::greeting();
        echo "<b>It's time to create a new color! </b><br>";
    }
}


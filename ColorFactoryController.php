<?php
namespace Colors;

/**
 * Class ColorFactoryController
 * @package Colors
 */
class ColorFactoryController
{
    /**
     * ColorFactoryController constructor.
     * @param ColorFactory $colorFactory
     */
    public function __construct(ColorFactory $colorFactory) {
        $this->colorFactory = $colorFactory;
    }

    /**
     * @param $formData
     * Takes User info to create a color using the model
     */
    public function createColor($formData)
    {
        if (!empty($formData['parentcolor1']) && !empty($formData['parentcolor2'])) {
            if(isset($formData['parentcolor1'])) {
                $parentColor1 = $this->findColor($formData['parentcolor1']);
                $formData['parentcolor1'] = $parentColor1['color_id'];
            }
            if(isset($formData['parentcolor2'])) {
                $parentColor2 = $this->findColor($formData['parentcolor2']);
                $formData['parentcolor2'] = $parentColor2['color_id'];
            }
            //Determine the new color's ID by adding the ID numbers of its parents
            $formData['colorID'] = $parentColor1['color_id'] + $parentColor2['color_id'];
            $formData['isprimary'] = 0;
        } else { //If it is a primary color, one has to be made up.
            $formData['colorID'] = rand($min = 1 , $max = 99999);
            $formData['isprimary'] = 1;
            $formData['parentcolor1'] = "";
            $formData['parentcolor2'] = "";
            echo "With no entries detected for the Parent Colors, this new color will be a primary color.<br>";
        }
        // If one or both of the entered parent colors doesn't exist,
        // The new color is primary.
        if (empty($formData['parentcolor1']) || empty($formData['parentcolor2'])) {
            $formData['colorID'] = rand($min = 1 , $max = 99999);
            $formData['isprimary'] = 1;
            $formData['parentcolor1'] = "";
            $formData['parentcolor2'] = "";
            echo "Because one or both of your supplied Parent Colors cannot be found, your new color will have to be a primary color.<br>";
        } else {
            echo "Because both Parent Colors were found, your new color is a secondary color.<br>";
        }
        //Removes whitespace and lower-cases to create the 'truecolorname'
        $formData['truecolorname'] = $this->colorFactory->trueColorName($formData['colorname']);

        if ($this->colorFactory->create($formData)) {
            echo "Congrats on the new Color!<br>";
        } else {
            echo "Sorry about not making your color. Try again later.<br>";
        };
    }
    /**
     * @param $formData
     * Takes User info to delete a color in the database
     */
    public function deleteColor($formData)
    {
        //Removes whitespace and lower-cases to create the 'truecolorname'
        $formData['truecolorname'] = $this->colorFactory->trueColorName($formData['colorname']);

        if ($this->colorFactory->delete($formData)) {
            echo "The color, " . $formData['colorname'] .", no longer exists!<br>";
        } else {
            echo "Please try deleting the color, " . $formData['colorname'] . ", at another time.<br>";
        };
    }
    /**
     * @param $formData
     * Takes User info to update a color in the database
     */
    public function updateColor($formData)
    {
        $originalID = $this->findColor($formData['colorname']);
        $formData['originalcolorID'] = $originalID['color_id'];

        if (!empty($formData['parentcolor1']) && !empty($formData['parentcolor2'])) {
            if (isset($formData['parentcolor1'])) {
                $parentColor1 = $this->findColor($formData['parentcolor1']);
                $formData['parentcolor1'] = $parentColor1['color_id'];
            }
            if (isset($formData['parentcolor2'])) {
                $parentColor2 = $this->findColor($formData['parentcolor2']);
                $formData['parentcolor2'] = $parentColor2['color_id'];
            }
            //Determine the updated color's ID by adding the ID numbers of its parents
            $formData['colorID'] = $parentColor1['color_id'] + $parentColor2['color_id'];
            $formData['isprimary'] = 0;
        } else { //If it is a primary color, one has to be made up.
            $formData['colorID'] = rand($min = 1 , $max = 99999);
            $formData['isprimary'] = 1;
            $formData['parentcolor1'] = "";
            $formData['parentcolor2'] = "";
            echo "With no entries detected for the Parent Colors, this updated color will be a primary color.<br>";
            // If one or both of the entered parent colors doesn't exist,
            // The updated color is primary.
            if (empty($formData['parentcolor1']) || empty($formData['parentcolor2'])) {
                $formData['colorID'] = rand($min = 1 , $max = 99999);
                $formData['isprimary'] = 1;
                $formData['parentcolor1'] = "";
                $formData['parentcolor2'] = "";
                echo "Because one or both of your supplied Parent Colors cannot be found, your updated color will have to be a primary color.<br>";
            } else {
                echo "Because both Parent Colors were found, your updated color is a secondary color.<br>";
            }
        }
        //Removes whitespace and lower-cases to create the 'truecolorname'
        $formData['truecolorname'] = $this->colorFactory->trueColorName($formData['colorname']);

        echo "<pre>";
        print_r ($formData);
        echo "</pre>";

        if ($this->colorFactory->update($formData)) {
            echo "Congrats!<br>";
        } else {
            echo "Sorry!<br>";
        };
    }
    /**
     * @param $color
     * @param null $sql
     * @return null
     * Checks the color provided to determine if it actually exists in the database
     */
    public function findColor($color,$sql = null)
    {
        $trueColor = $this->colorFactory->trueColorName($color);
        if (is_null($sql)) {
            $findColor = $this->colorFactory->find($trueColor);
        } else {
            $findColor = $this->colorFactory->find($trueColor,$sql);
        }

        return $findColor;
    }
    /**
     * @param $colorname
     * @return bool
     * Checks database to see if color name is original
     */
    public function originalColor($colorname)
    {
        $oldColor = $this->colorFactory->find($colorname);
        if ($oldColor === NULL) { //This is an original color name

            return true;
        } else{ //The color already exists and is NOT original

            return false;
        }
    }

    /**
     * @return null
     */
    public function findAll()
    {
        $colorListings = $this->colorFactory->find("","SELECT * FROM color_factory");

        return $colorListings;
    }

    /**
     * Grabs the submitted form data and sends it to $data
     * @return mixed
     */
    public function getPost()
    {
        $post_data = file_get_contents('php://input');
        foreach (explode('&',$post_data) as $pair) {
            list ($k,$v) = explode ('=',$pair);
            $data[$k] = $v;
        }

        return $data;
    }
}


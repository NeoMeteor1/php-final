<?php
namespace Colors;

/**
 * Class ColorFactory
 * @package Colors
 */
class ColorFactory
{
    private $colorFactoryRepo;

    /**
     * ColorFactory constructor.
     * @param ColorFactoryRepo $colorFactoryRepo
     */
    public function __construct(ColorFactoryRepo $colorFactoryRepo)
    {
        $this->colorFactoryRepo = $colorFactoryRepo;
    }

    /**
     * @param $formData
     * @return bool
     * Takes input from User to Create a color
     */
    public function create($formData)
    {

        return $this->colorFactoryRepo->createColor($formData);
    }

    /**
     * @param $formData
     * @return bool|null
     * Takes input from User to Delete a color
     */
    public function delete($formData)
    {

        return $this->colorFactoryRepo->deleteColor($formData);
    }

    /**
     * @param $formData
     * @return bool
     * Takes input from User to Update a color
     */
    public function update($formData)
    {

        return $this->colorFactoryRepo->updateColor($formData);
    }

    /**
     * @param string $color
     * @param string $sql
     * @return null
     * Takes input to Find a color
     */
    public function find($color = "", $sql = "SELECT * FROM color_factory WHERE true_color_name")
    {

        return $this->colorFactoryRepo->FindColor($color,$sql);
    }

    /**
     * @param $colorname
     * @return bool
     * Ensures color name exists in database
     */
    public function isValid($colorname)
    {

        return $this->colorFactoryRepo->isValidColor($colorname);
    }

    /**
     * @param $colorname
     * @return bool
     * Ensures color name is original and NOT already in the database
     */
    public function isValidName($colorname)
    {

        return $this->colorFactoryRepo->isValidColorName($colorname);
    }

    /**
     * @param $color
     * @return mixed|string
     */
    public function trueColorName($color)
    {

        return $this->colorFactoryRepo->trueName($color);
    }
}


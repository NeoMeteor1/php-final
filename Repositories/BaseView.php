<?php

Namespace Colors;

/**
 * Class BaseView
 * @package Colors
 */
class BaseView
{
    /**
     * BaseView constructor.
     * @param ColorFactoryController $cfController
     */
    public function __construct(ColorFactoryController $cfController) {
        $this->cfController = $cfController;
    }

    /**
     * Displays greeting meesage to User at random from array of possible choices
     */
    public function greeting(){
        $greeting = array(1 =>  'How about trying this fun station?',
                                'Why not try this out?',
                                'Looking for something fun?',
                                'Wanna try something cool?',
                                'This looks pretty neat!',
                                'Another innovation from Color Factory!',
        );
        echo '<b>' . $greeting[rand(1,count($greeting))] . "</b><br>";
    }

    /**
     * @param null $action
     * @param bool $parent
     * @return string
     */
    public function featureView($action = null, $parent = true)
    {
        $html = '<div><h3>' . ucfirst(strtolower($action)) . ' Color</h3>
                <form name =' . ucfirst(strtolower($action)) . ' action="?action=' . strtolower($action) . '" method="POST">
                <label>Color Name: </label><input id="colorname" name="colorname" type="text" value=""/>
                <br>';

        // For any feature that may not want/require the parent colors as an option
        if ($parent === true) {
            $html = $html . '<label > Parent Color 1: </label ><input id = "parentcolor1" name = "parentcolor1" type = "text" value = "" />
                            <br >
                            <label > Parent Color 2: </label ><input id = "parentcolor2" name = "parentcolor2" type = "text" value = "" />';
            }
        $html = $html . '<br>
                        <input id ="submit" type="submit" name="submit" value="' . ucfirst(strtolower($action)) . '"/>
                        </form>
                        </div>';

        return $html;
    }
}


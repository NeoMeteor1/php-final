<?php

namespace Colors;

use Hamcrest\Core\Is;
use PDO;
use PDOException;

/**
 * Class ColorFactoryRepo
 * @package Colors
 * This class acts as the method repo for the model/class, "ColorFactory"
 */
class ColorFactoryRepo
{
    /**
     * @type array $errors stores error messages, but is also used as a counter
     */
    private $errors;

    /**
     * @property $connection is used to store the PDO instance for the model
     */
    private $connection;

    /**
     * ColorFactoryRepo constructor.
     * @param $cfServer
     * @param $cfUser
     * @param $cfPass
     * @param $cfDatabase
     */
    public function __construct($cfServer, $cfUser, $cfPass, $cfDatabase)
    {
        $this->cfServer = $cfServer;
        $this->cfUser = $cfUser;
        $this->cfPass = $cfPass;
        $this->cfDatabase = $cfDatabase;
        $this->connect();
    }

    /**
     * Sets up the PDO object in order to communicate with the database
     */
    private function connect()
    {
        $this->connection = new PDO("mysql:host=$this->cfServer;dbname=$this->cfDatabase", $this->cfUser, $this->cfPass);
        // set the PDO error mode to exception
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param $formData
     * @return bool
     * Takes input from User to Create a color
     */
    public function createColor($formData)
    {
        if ($formData['isprimary'] === 0) {
            $sql = "INSERT INTO color_factory (color_id, color_name, true_color_name, is_primary, parent_color_1, parent_color_2)
            VALUES (
            '" . $formData['colorID'] . "',
            '" . $formData['colorname'] . "',
            '" . $formData['truecolorname'] . "',
            '" . $formData['isprimary'] . "',
            '" . $formData['parentcolor1'] . "',
            '" . $formData['parentcolor2'] . "')";
        } else {
            $sql = "INSERT INTO color_factory (color_id, color_name, true_color_name, is_primary)
            VALUES (
            '" . $formData['colorID'] . "',
            '" . $formData['colorname'] . "',
            '" . $formData['truecolorname'] . "',
            '" . $formData['isprimary'] .  "')";
        }

        try {
            if ($createColor = $this->connection->exec($sql)) {
                echo "<br>New color, " . $formData['colorname'] . " created successfully!<br>";

                return true;
            } else {
                echo "<br>Unable to create desired color! Try again soon!<br>";

                return false;
            }
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();

            return false;
        }
    }

    /**
     * @param $formData
     * @return bool|null
     * Takes input from User to Delete a color
     */
    public function deleteColor($formData)
    {
        $sql = "DELETE FROM color_factory WHERE true_color_name";

        try {
            // Attempt to connect and get past selections, adding them to an array
            if ($this->connection->exec($sql . " = '" . $formData['truecolorname'] . "'")) {
                echo "<br>Successfully deleted the color, " . $formData['colorname'] . "!<br>";

                return true;
            } else {
                echo "<br>Unable to delete specified color, "  . $formData['colorname'] . "!<br>";

                return false;
            }
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();

            return null;
        }
    }

    /**
     * @param $formData
     * @return bool
     * Takes input from User to Update a color
     */
    public function updateColor($formData)
    {
        if ($formData['isprimary'] === 0) {
            $sql = "UPDATE color_factory " .
                "SET color_id = " . $formData['colorID'] . ", color_name = '" . $formData['colorname'] . "', true_color_name = '" . $formData['truecolorname'] . "', is_primary = " . $formData['isprimary'] . ", parent_color_1 = " . $formData['parentcolor1'] . ", parent_color_2 = " . $formData['parentcolor2'] . " " .
                "WHERE color_id = " . $formData['originalcolorID'];
        } else {
            $sql = "UPDATE color_factory " .
                "SET color_id = " . $formData['colorID'] . ", color_name = '" . $formData['colorname'] . "', true_color_name = '" . $formData['truecolorname'] . "', is_primary = " . $formData['isprimary'] . ", parent_color_1 = null, parent_color_2 = null " .
                "WHERE color_id = " . $formData['originalcolorID'];
        }
        try {
            if ($updateColor = $this->connection->exec($sql)) {
                echo "<br>The color, " . $formData['colorname'] . ", was updated successfully!<br>";

                return true;
            } else {
                echo "<br>Unable to update the color, " . $formData['colorname'] . "! Try again soon!<br>";

                return false;
            }
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();

            return false;
        }
    }


    /**
     * @param string $color
     * @param string $sql
     * @return null
     * Takes input to Find a color
     */
    public function findColor($color = "", $sql = "SELECT * FROM color_factory WHERE true_color_name")
    {
        try {
            // Attempt to connect and get past selections, adding them to an array
            if (!empty($color)) {
                $sql = $sql . " = '" . $color . "'";
                $foundColor = $this->connection->query($sql);
                $foundColor = $foundColor->fetch();
            } else {
                $foundColor = $this->connection->query($sql);
                $foundColor = $foundColor->fetchAll();
            }
            if (empty($foundColor)) {
                $foundColor = null;
                echo "The true color value, " . $color . ", cannot be found.<br>";

                return $foundColor;
            } else {
                echo "Successfully found the true color value, " . $color . ".<br>";

                return $foundColor;
            }
        } catch(PDOException $e) {
            $foundColor = null;
            echo $sql . "<br>" . $e->getMessage();

            return $foundColor;
        }
    }

    /**
     * @return int
     * Model validation - Checks the validity of the supplied color
     */
    public function errors()
    {

        return $this->errors;
    }

    //Ensures color name exists in database
    /**
     * @param $colorname
     * @return bool
     */
    public function isValidColor($colorname)
    {
        if ($this->validateColor($colorname)) {
            echo $colorname . ' is a valid color.<br>';

            return true;
        } else {
            echo $colorname . ' is not a valid color.<br>';

            return false;
        }
    }

    /**
     * @param $colorname
     * @return bool
     * Ensures color name is original and NOT already in the database
     */
    public function isValidColorName($colorname)
    {
        if ($this->validateColorName($colorname)) {
            echo $colorname . ' already exists and is not an available color name.<br>';

            return false;
        } else {
            echo $colorname . ' is available to use as your new color\'s name.<br>';

            return true;
        }
    }

    /**
     * @param $colorname
     * @return bool
     * Checks to see if the color name supplied exists in the database
     */
    private function validateColor($colorname)
    {
        $validColor= $this->trueName($colorname);

        $validity = $this->findColor($validColor,'SELECT * FROM color_factory WHERE true_color_name');

        if ($validity === null) {
            $this->errors['invalid_color'][$validColor] = $validColor . ' is not a valid color.';
        }

        return count($this->errors) === 0;
    }

    /**
     * @param $colorname
     * @return bool
     * Checks to see if the color name supplied exists in the database
     */
    private function validateColorName($colorname)
    {
        $validColor= $this->trueName($colorname);

        $validity = $this->findColor($validColor,"SELECT * FROM color_factory WHERE true_color_name");

        if ($validity === null) {
            $this->errors['invalid_color_name'][$validColor] = $validColor . ' already exists and is not an available color name.';
        }

        return count($this->errors) === 0;
    }

    /**
     * @param $color
     * @return mixed|string
     * Takes input Color name and breaks it down
     * to match what the true_color_name value would be in the database
     */
    public function trueName($color) {
        $trueColor= str_replace(' ', '', $color);
        $trueColor= str_replace('-', '', $trueColor);
        $trueColor = preg_replace('/[^A-Za-z0-9\-]/', '', $trueColor);
        $trueColor = strtolower($trueColor);

        return $trueColor;
    }
}

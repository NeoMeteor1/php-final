<?php

$faker = Faker\Factory::create();
$name = $faker->colorName;
$I = new AcceptanceTester($scenario);
$I->wantTo('create a new color (without parent colors) and save to the database');
$I->amOnPage('');
$I->fillField('colorname', $name);
$I->click('Create');
$I->expect('the new color to be created successfully');
$I->see('Congrats on the new color!');

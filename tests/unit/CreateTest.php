<?php
namespace Colors;

use Mockery;
use Faker;

require_once dirname(__FILE__) . '/../../ColorFactory.php';
require_once dirname(__FILE__) . '/../../Repositories/ColorFactoryRepo.php';

/**
 * Class ColorFactoryTest
 * @package Colors
 * @property Faker\Factory faker
 * @property array formData
 * @property Mockery\MockInterface mockRepo
 * @property ColorFactoryRepo testDB
 * @property ColorFactory cfTest
 * @property ColorFactory cfDBTest
 */
class CreateTest extends \Codeception\TestCase\Test
{

    private $faker;
    private $formData;
    public $mockRepo;
    public $testDB;
    public $cfTest;
    public $cfDBTest;

    protected $cfServer = "homestead.app";
    protected $cfUser = "homestead";
    protected $cfPass = "secret";
    protected $cfDatabase = "homestead";

    protected function _before()
    {
        $this->faker = Faker\Factory::create();
        $this->mockRepo = Mockery::mock('\Colors\ColorFactoryRepo');
        $this->testDB = new ColorFactoryRepo($this->cfServer, $this->cfUser, $this->cfPass, $this->cfDatabase);
        $this->cfTest = new ColorFactory($this->mockRepo);
        $this->cfDBTest = new ColorFactory($this->testDB);

        $this->formData['colorname'] = $this->faker->colorName;
        $this->formData['truecolorname'] = $this->trueName($this->formData['colorname']);
        $this->formData['isprimary'] = $this->faker->numberBetween($min = 0, $max = 1);
        if ($this->formData['isprimary'] === 0) {
            $this->formData['parentcolor1'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['parentcolor2'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['colorID'] = $this->formData['parentcolor1'] + $this->formData['parentcolor2'];
        } else {
            $this->formData['colorID'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['parentcolor1'] = null;
            $this->formData['parentcolor2'] = null;
        }
    }

//    protected function _after()
//    {
//    }

    /**
     * Codeception Unit Test for ColorFactory create() method
     */
    public function testCreate()
    {
        $this->mockRepo
            ->shouldReceive('createColor')
            ->with($this->formData)
            ->once()
            ->andReturn(true);

        $this->assertTrue($this->cfTest->create($this->formData));

        Mockery::resetContainer();
    }
    /**
     * @param $color
     * @return mixed|string
     */
    public function trueName($color)
    {
        $trueColor= str_replace(' ', '', $color);
        $trueColor= str_replace('-', '', $trueColor);
        $trueColor = preg_replace('/[^A-Za-z0-9\-]/', '', $trueColor);
        $trueColor = strtolower($trueColor);

        return $trueColor;
    }
}
<?php

namespace Colors;

use PHPUnit_Framework_TestCase;
use Mockery;
use Faker;

require_once dirname(__FILE__) . '/../ColorFactory.php';
require_once dirname(__FILE__) . '/../Repositories/ColorFactoryRepo.php';

/**
 * Class ColorFactoryTest
 * @package Colors
 * @property Faker\Factory faker
 * @property array formData
 * @property Mockery\MockInterface mockRepo
 * @property ColorFactoryRepo testDB
 * @property ColorFactory cfTest
 * @property ColorFactory cfDBTest
 */
class ColorFactoryTest extends PHPUnit_Framework_TestCase
{
    private $faker;
    private $formData;
    public $mockRepo;
    public $testDB;
    public $cfTest;
    public $cfDBTest;

    protected $cfServer = "homestead.app";
    protected $cfUser = "homestead";
    protected $cfPass = "secret";
    protected $cfDatabase = "homestead";

    /**
     * The setup for the PHPUnit test
     */
    public function setUp()
    {
        $this->faker = Faker\Factory::create();
        $this->mockRepo = Mockery::mock('\Colors\ColorFactoryRepo');
        $this->testDB = new ColorFactoryRepo($this->cfServer, $this->cfUser, $this->cfPass, $this->cfDatabase);
        $this->cfTest = new ColorFactory($this->mockRepo);
        $this->cfDBTest = new ColorFactory($this->testDB);

        $this->formData['colorname'] = $this->faker->colorName;
        $this->formData['truecolorname'] = $this->trueName($this->formData['colorname']);
        $this->formData['isprimary'] = $this->faker->numberBetween($min = 0, $max = 1);
        if ($this->formData['isprimary'] === 0) {
            $this->formData['parentcolor1'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['parentcolor2'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['colorID'] = $this->formData['parentcolor1'] + $this->formData['parentcolor2'];
        } else {
            $this->formData['colorID'] = $this->faker->numberBetween($min = 1, $max = 99999);
            $this->formData['parentcolor1'] = null;
            $this->formData['parentcolor2'] = null;
        }
    }

    /**
     * PHPUnit Test for the Model "ColorFactory"
     */
    public function testModel()
    {
        // 4.6.1: The validation should definitely fail.
        // 4.6.2: An error message accompanies the "failure".
//        $this->assertFalse($this->cfDBTest->isValidName('Rerg'));

        // 4.6.3: The validation should definitely pass.
        // This should not be a valid name (to use in the create() method)
        // as this color name already exists in the database.
        $this->assertFalse($this->cfDBTest->isValidName('Red'));

        // 4.6.3: The validation should definitely pass.
        // This should be a valid name (to use in the create() method)
        // as this color name does NOT exist in the database.
        $this->assertTrue($this->cfDBTest->isValidName('Gargamel'));

        // 4.6.1: The validation should definitely pass.
        // 4.6.2: This should NOT be the name of an invalid color that doesn't exist in the database.
        // An error message accompanies the "failure".
//        $this->assertTrue($this->cfDBTest->isValid('Gargamel'));

        // 4.6.3: The validation should definitely pass.
        // This should be the name of a valid, currently existing color in the database
        // This is used for validating color names when using the update() method.
        $this->assertTrue($this->cfDBTest->isValid('Red'));

        // 4.6.3: The validation should definitely pass.
        // This should NOT be the name of a valid, currently existing color in the database.
        // This is used for validating color names when using the update() method.
        $this->assertFalse($this->cfDBTest->isValid('Gargamel'));

        echo "The database currently has " . count($this->cfDBTest->find()) . " colors. <br>";
    }

    /**
     * PHPUnit Test for ColorFactory database
     */
    public function testDatabase()
    {
        // Testing the create() method
        print_r($this->formData);

        $this->assertTrue($this->cfDBTest->create($this->formData));

        echo "The database currently has " . count($this->cfDBTest->find()) . " colors. <br>";

        // Testing the update() method
        print_r($this->formData);

        $this->formData['isprimary'] = 0;
        $this->formData['originalcolorID'] = $this->formData['colorID'];
        $this->formData['parentcolor1'] = $this->faker->numberBetween($min = 1, $max = 99999);
        $this->formData['parentcolor2'] = $this->faker->numberBetween($min = 1, $max = 99999);
        $this->formData['colorID'] = $this->formData['parentcolor1'] + $this->formData['parentcolor2'];

        print_r($this->formData);

        $this->assertTrue($this->cfDBTest->update($this->formData));

        echo "The database currently has " . count($this->cfDBTest->find()) . " colors. <br>";

        // Testing the delete() method
        print_r($this->formData);

        $this->assertTrue($this->cfDBTest->delete($this->formData));

        echo "The database currently has " . count($this->cfDBTest->find()) . " colors. <br>";

    }

    /**
     * PHPUnit Test for ColorFactory method, create()
     */
    public function testCreate()
    {
        $this->mockRepo
            ->shouldReceive('createColor')
            ->with($this->formData)
            ->once()
            ->andReturn(true);

        $this->assertTrue($this->cfTest->create($this->formData));

        Mockery::resetContainer();
    }

    /**
     * PHPUnit Test for ColorFactory method, delete()
     */
    public function testDelete()
    {
        $this->mockRepo
            ->shouldReceive('deleteColor')
            ->with($this->formData)
            ->once()
            ->andReturn(true);

        $this->assertTrue($this->cfTest->delete($this->formData));

        Mockery::resetContainer();
    }

    /**
     * PHPUnit Test for ColorFactory method, update()
     */
    public function testUpdate()
    {
        $this->mockRepo
            ->shouldReceive('updateColor')
            ->with($this->formData)
            ->once()
            ->andReturn(true);

        $this->assertTrue($this->cfTest->update($this->formData));

        Mockery::resetContainer();
    }

    /**
     * PHPUnit Test for ColorFactory method, find()
     */
    public function testFind()
    {
        $this->mockRepo
            ->shouldReceive('findColor')
            ->with($this->formData['colorname'])
            ->once()
            ->andReturn(true);

        $this->assertTrue($this->cfTest->find($this->formData['colorname']));

        Mockery::resetContainer();
    }

    /**
     * @param $color
     * @return mixed|string
     */
    public function trueName($color)
    {
        $trueColor= str_replace(' ', '', $color);
        $trueColor= str_replace('-', '', $trueColor);
        $trueColor = preg_replace('/[^A-Za-z0-9\-]/', '', $trueColor);
        $trueColor = strtolower($trueColor);

        return $trueColor;
    }
}


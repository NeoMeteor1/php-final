<?php
namespace Colors;

/**
 * Class ColorFactoryUpdate
 * @package Colors
 */
class ColorFactoryUpdate extends BaseView
{
    /**
     * Overrides parent method to add an update-centric sentence
     */
    public function greeting(){
        parent::greeting();
        echo "<b>It's time to jazz up one of your colors! </b><br>";
    }
}

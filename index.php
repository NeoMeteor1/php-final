<?php

include_once 'Repositories/BaseView.php';
include_once 'Repositories/ColorFactoryRepo.php';
include_once 'ColorFactory.php';
include_once 'ColorFactoryController.php';
include_once 'ColorFactoryCreate.php';
include_once 'ColorFactoryDelete.php';
include_once 'ColorFactoryUpdate.php';


use Colors\ColorFactory;
use Colors\ColorFactoryController;
use Colors\ColorFactoryCreate;
use Colors\ColorFactoryDelete;
use Colors\ColorFactoryUpdate;
use Colors\ColorFactoryRepo;

$cfServer = "homestead.app";
$cfUser = "homestead";
$cfPass = "secret";
$cfDatabase = "homestead";
$repo = new ColorFactoryRepo($cfServer, $cfUser, $cfPass, $cfDatabase);
$color = new ColorFactory($repo);
$colorController = new ColorFactoryController($color);
$colorCreate = new ColorFactoryCreate($colorController);
$colorDelete = new ColorFactoryDelete($colorController);
$colorUpdate = new ColorFactoryUpdate($colorController);

echo $colorCreate->featureView("Create") . $colorCreate->greeting();
echo $colorDelete->featureView("Delete", false) . $colorDelete->greeting();
echo $colorUpdate->featureView("Update") . $colorUpdate->greeting();

if (isset($_GET['action']) && $_GET['action'] == 'create') {
    $data = $colorCreate->cfController->getPost();
    $colorCreate->cfController->createColor($data);
}
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    $data = $colorDelete->cfController->getPost();
    $colorDelete->cfController->deleteColor($data);
}
if (isset($_GET['action']) && $_GET['action'] == 'update') {
    $data = $colorUpdate->cfController->getPost();
    $colorUpdate->cfController->updateColor($data);
}


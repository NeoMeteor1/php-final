<?php
namespace Colors;

/**
 * Class ColorFactoryDelete
 * @package Colors
 */
class ColorFactoryDelete extends BaseView
{
    /**
     * Overrides parent method to add a delete-centric sentence
     */
    public function greeting(){
        parent::greeting();
        echo "<b>Let's get rid of one these old colors! </b><br>";
    }
}
